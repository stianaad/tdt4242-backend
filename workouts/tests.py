from django.test import TestCase
from workouts import permissions
from workouts.models import Workout
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from workouts.serializers import WorkoutSerializer

class WorkoutTest:
    def __init__(self, name="test", date="2/2/2020", notes="some notes", owner="python", visibility="PU"):
        self.name = name
        self.date = date
        self.notes = notes
        self.owner = owner
        self.visibility = visibility

class Obj:
    def __init__(self, ownerValue='', workout='', visibility='PU'):
        self.owner = ownerValue
        self.workout = workout
        self.visibility = visibility

class Request:
    def __init__(self, user, method="GET", data={}):
        self.user = user
        self.method = method
        self.data = data

class IsCoachAndVisibleToCoachTest:
    def __init__(self, owner):
        self.owner = owner

class Coach:
    def __init__(self, coach):
        self.coach = coach

class Test(TestCase):

    def test_IsOwner(self):
        print("test_IsOwner")
        correctName = "Python"
        wrongName = "Java"
        correctRes = permissions.IsOwner().has_object_permission(Request(correctName), "view", Obj(correctName))
        wrongRes = permissions.IsOwner().has_object_permission(Request(wrongName), "view", Obj(correctName))
        self.assertEqual(correctRes, True)
        self.assertEqual(wrongRes, False)

    def test_IsOwnerOfWorkout_has_permisson(self):
        print("test_IsOwnerOfWorkout_has_permisson")
        user = get_user_model()(username="Python", email="email@homtail", phone_number="phone_number", country="country", city="city", street_address="street_address")
        request = Request(user, "POST", {"workout": "http://localhost:8000/api/users/2/"})
        anotherRequestWithoutPOST = Request(user, "GET", {"workout": "http://localhost:8000/api/users/2/"})
        requestWithoutWorkout = Request(user, "POST", {})
        user.save()
        Workout.objects.create(name="Java", date="2020-10-10 10:10:10", notes="some notes", owner=user, visibility="Public")
        Workout.objects.create(name="Python", date="2020-10-10 10:10:10", notes="some notes", owner=user, visibility="Public")

        correctRes = permissions.IsOwnerOfWorkout.has_permission(self, request=request,view= "view")
        correctRes2 = permissions.IsOwnerOfWorkout.has_permission(self, request=anotherRequestWithoutPOST,view= "view")
        wrongRes = permissions.IsOwnerOfWorkout.has_permission(self, request=requestWithoutWorkout,view= "view")

        self.assertEqual(correctRes, True)
        self.assertEqual(correctRes2, True)
        self.assertEqual(wrongRes, False)

    def test_IsOwnerOfWorkout_has_object_permission(self):
        print("test_IsOwnerOfWorkout_has_object_permission")
        correctName = "Python"
        wrongName = "Java"

        correctRes = permissions.IsOwnerOfWorkout.has_object_permission(self, Request(correctName), "view", Obj(correctName, WorkoutTest(owner=correctName)))
        wrongRes = permissions.IsOwnerOfWorkout.has_object_permission(self, Request(wrongName), "view", Obj(correctName, WorkoutTest(owner=correctName)))
        self.assertEqual(correctRes, True)
        self.assertEqual(wrongRes, False)
        
    def test_IsCoachAndVisibleToCoach_has_object_permission(self):
        print("test_IsCoachAndVisibleToCoach_has_object_permission")
        correctName = "Python"
        wrongName = "Java"
        correctRes = permissions.IsCoachAndVisibleToCoach.has_object_permission(self, Request(correctName), "view", IsCoachAndVisibleToCoachTest(Coach(correctName)))
        wrongRes = permissions.IsCoachAndVisibleToCoach.has_object_permission(self, Request(wrongName), "view", IsCoachAndVisibleToCoachTest(Coach(correctName)))
        self.assertEqual(correctRes, True)
        self.assertEqual(wrongRes, False)

    def test_IsCoachOfWorkoutAndVisibleToCoach_has_object_permission(self):
        print("test_IsCoachOfWorkoutAndVisibleToCoach_has_object_permission")
        correctName = "Python"
        coach = IsCoachAndVisibleToCoachTest(Coach(correctName))
        obj = Obj(correctName, coach)
        correctRes = permissions.IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(self, Request(correctName), "view", obj)
        self.assertEqual(correctRes, True)

    def test_IsPublic(self):
      print("test_IsPublic")
      correctVisibility = "PU"
      wrongVisibility = "PRIVATE"
      correctRes = permissions.IsPublic.has_object_permission(self, "request", "view", Obj(visibility=correctVisibility))
      wrongRes = permissions.IsPublic.has_object_permission(self, "request", "view", Obj(visibility=wrongVisibility))
      self.assertEqual(correctRes, True)
      self.assertEqual(wrongRes, False)

    def test_IsWorkoutPublic(self):
      print("test_IsWorkoutPublic")
      correctVisibility = "PU"
      wrongVisibility = "PRIVATE"
      correctObj = Obj(workout=WorkoutTest(visibility=correctVisibility))
      wrongObj = Obj(workout=WorkoutTest(visibility=wrongVisibility))

      correctRes = permissions.IsWorkoutPublic.has_object_permission(self, "request", "view", correctObj)
      wrongRes = permissions.IsWorkoutPublic.has_object_permission(self, "request", "view", wrongObj)
      
      self.assertEqual(correctRes, True)
      self.assertEqual(wrongRes, False)

    def test_IsReadOnly(self):
      print("test_IsReadOnly")
      correctMethod = "GET"
      wrongMethod = "POST"

      correctRes = permissions.IsReadOnly.has_object_permission(self, Request(user="test", method=correctMethod), "view", "obj")
      wrongRes = permissions.IsReadOnly.has_object_permission(self, Request(user="test", method=wrongMethod), "view", "obj")

      self.assertEqual(correctRes, True)
      self.assertEqual(wrongRes, False)

class TestSerializer(TestCase): 
    def test_update(self):
        print("test_update")
        user = get_user_model()(username="Python", email="email@homtail", phone_number="phone_number", country="country", city="city", street_address="street_address")
        user.save()
        w_obj = Workout.objects.create(name="Java", date="2020-10-10 10:10:10", notes="some notes", owner=user, visibility="Public")
        w_obj.save()
        validated_data = {
            "url": "testurl.com",
            "id": "1",
            "name":"Java",
            "date": "2020-10-10 10:10:10",
            "notes": "andre notater",
            "owner": user,
            "owner_username": "Python",
            "visibility": "Private",
            "exercise_instances": [],
            "files": []
        }
        res = WorkoutSerializer.update(self, w_obj, validated_data)
        self.assertEqual(res.notes, "andre notater")
      