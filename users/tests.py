from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from users import serializers, models
from users.models import User
from django.db.utils import IntegrityError
from rest_framework import serializers as s
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from comments.models import Comment
from random import choice
from string import ascii_lowercase
import json
import copy
from django.core.files.uploadedfile import SimpleUploadedFile

class CreateUserTest(TestCase):
    def setUp(self):
        validated_data = {
            "username": "username", 
            "password": "password",
            "password1": "password1",
            "email": "",
            "phone_number": 123, 
            "country": "country",
            "city": "", 
            "street_address": ""
        }
        serializers.UserSerializer.create(self, validated_data)

    def test_create(self): 
        user = User.objects.get(username="username")
        self.assertEqual(user.phone_number, "123")

    def test_equal_username(self):
        validated_data = {
            "username": "username", 
            "password": "password",
            "password1": "password1",
            "email": "",
            "phone_number": 123, 
            "country": "country",
            "city": "", 
            "street_address": ""
        }
        with self.assertRaises(IntegrityError):
            serializers.UserSerializer.create(self, validated_data)

    
class TestPassord(TestCase):
    def test_validate_password(self):
        validated_data = {
            "username": "username", 
            "password": "password",
            "password1": "password"
        }

        res = serializers.UserSerializer(data=validated_data).validate_password(value="password")
        self.assertEqual("password", res)

        validated_data2 = {
            "username": "username", 
            "password": "password",
            "password1": "password1"
        }

        with self.assertRaises(s.ValidationError):
            serializers.UserSerializer(data=validated_data2).validate_password(value="password")

test_list = [
    {"username": "test", "email": "test@hotmail.com", "password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "Norge", "city": "Bergen", "street_address": "Uib"},
    {"username": "/test", "email": "test", "password": "", "password1": "", "phone_number": "", "country": "", "city": "", "street_address": "Uib"},
    {"username": '', "email": "", "password": "", "password1": "", "phone_number": 12354678, "country": "Norge", "city": "Bergen", "street_address": ""},
    {"username": '', "email": "test@hotmail.com",  "password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "", "city": "", "street_address": ""},
    {"username": "/test", "email": "test", "password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "Norge", "city": "Bergen", "street_address": ""},
    {"username": "test", "email": "","password": "", "password1": "test#!12345", "phone_number": "", "country": "", "city": "Bergen", "street_address": "Uib"},
    {"username": "test", "email": "","password": "test#!12345", "password1": "", "phone_number": 12354678, "country": "Norge", "city": "", "street_address": ""},
    {"username": "/test", "email": "test@hotmail.com","password": "", "password1": "", "phone_number": "", "country": "Norge", "city": "Bergen", "street_address": "Uib"},
    {"username": '', "email": "test","password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "", "city": "Bergen", "street_address": "Uib"},
    {"username": "test", "email": "test","password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "Norge", "city": "Bergen", "street_address": "Uib"},
    {"username": "/test", "email": "","password": "test#!12345", "password1": "test#!12345", "phone_number": 12354678, "country": "Norge", "city": "Bergen", "street_address": "Uib"},
]
success = 201
error = 400
#2-way domain testing
class Testing_two_way_in_parameter_order(TestCase):
  def test_two_way_parameter_order(self):
    self.client = Client()
    print("test_two_way")
    #Loop through each row with the test list
    for row in test_list:
      statusCode = success
      # Check if the input is wrong or empty, then return 400
      if row['username'] == '/test' or row['username'] == '' or row['email'] == 'test' or row['email'] == '' or row['password'] == '' or row['password1'] == '':
        statusCode = error
      res = self.client.post('/api/users/', row)
      self.assertEqual(res.status_code, statusCode)


'''
class VisibilityTest(TestCase):
  def setUp(self):
    self.client = Client()
    self.coach = User.objects.create(username="coach")
    self.coach.set_password('12345')
    self.coach.save()
    self.athlete = User.objects.create(username="athlete", coach=self.coach)
    self.athlete.set_password('12345')
    self.athlete.save()
    self.other = User.objects.create(username="other")


  def test_athlete_sees_workouts(self):
    response = self.client.post('/api/token/', {'username': 'athlete', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Athlete can see owned private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete can see public workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete cannot see private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

    # Athlete cannot see coach-visibility workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

  def test_coach_sees_workouts(self):
    response = self.client.post('/api/token/', {'username': 'coach', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Coach can see public athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see coach-visibility athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see public other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach cannot see private athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    #self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    #self.assertEqual(response.status_code, 403)

    # Coach cannot see private other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)
    
    # Coach cannot see coach-visibility other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)
'''


validWorkoutForm = {
	"name":"My Workout",
	"date":"2021-03-11T12:30",
	"notes":"Test notes",
	"visibility":"PR",
	"exercise_instances":[],
	"files":[]
}

athlete = {'username': 'athlete','email': 'athlete@hotmail.com','password': 'athlete#!12345','password1': 'athlete#!12345','phone_number': 12354678,'country': 'Norge','city': 'Bergen','street_address': 'Uib'}
anders= {"username":"Anders","password":"anders"}
stian = {"username":"Stian","password":"stian"}
successStatusCode = 200
errorStatusCode = 403
class FR5(TestCase):
    def setUp(self):
        self.client = Client()
        ##Coach object
        self.coach = User.objects.create(username="Stian")
        self.coach.set_password('stian')
        self.coach.save()
        #Athlete object
        self.anders = User.objects.create(username="Anders", coach=self.coach)
        self.anders.set_password("anders")
        self.anders.save()
        #Another athlete without coach
        self.sara = User.objects.create(username="Sara")

    def test_athlete_see_another_person_create_coach_workout(self):
        #Login
        self.response = self.client.post("/api/token/",anders)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="CO", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, errorStatusCode)
        self.assertEqual(responseComment.status_code, errorStatusCode)
        self.assertEqual(responseFile.status_code, errorStatusCode)

    def test_athlete_see_own_private_workout(self):
        #Login
        self.response = self.client.post("/api/token/",anders)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.anders, visibility="PR", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.anders, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.anders,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, successStatusCode)
        self.assertEqual(responseComment.status_code, successStatusCode)
        self.assertEqual(responseFile.status_code, successStatusCode)


    def test_athlete_see_public_workout(self):
        #Login
        self.response = self.client.post("/api/token/",anders)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #See public workout
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="PU", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, successStatusCode)
        self.assertEqual(responseComment.status_code, successStatusCode)
        self.assertEqual(responseFile.status_code, successStatusCode)

    def test_athlete_see_private_workout_for_another_person(self):
        #Login
        self.response = self.client.post("/api/token/",anders)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #See private workout for another person
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="PR", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, errorStatusCode)
        self.assertEqual(responseComment.status_code, errorStatusCode)
        self.assertEqual(responseFile.status_code, errorStatusCode)


    def test_coach_see_public_workout(self):
        #Login
        self.response = self.client.post("/api/token/",stian)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="PU", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, successStatusCode)
        self.assertEqual(responseComment.status_code, successStatusCode)
        self.assertEqual(responseFile.status_code, successStatusCode)


    def test_coach_see_coachworkouts_from_athlete(self):
        #Login
        self.response = self.client.post("/api/token/",stian)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.anders, visibility="CO", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.anders, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.anders,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, successStatusCode)
        self.assertEqual(responseComment.status_code, successStatusCode)
        self.assertEqual(responseFile.status_code, successStatusCode)

    def test_coach_see_private_workout_from_athlete(self):
        #Login
        self.response = self.client.post("/api/token/",stian)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.anders, visibility="PR", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.anders, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.anders,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        #self.assertEqual(responseWorkout.status_code, errorStatusCode)
        self.assertEqual(responseComment.status_code, errorStatusCode)
        #self.assertEqual(responseFile.status_code, errorStatusCode)

    def test_coach_see_private_workout_from_other_person(self):
        #Login
        self.response = self.client.post("/api/token/",stian)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="PR", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, errorStatusCode)
        self.assertEqual(responseComment.status_code, errorStatusCode)
        self.assertEqual(responseFile.status_code, errorStatusCode)

    def test_coach_see_coachworkouts_from_other_person(self):
        #Login
        self.response = self.client.post("/api/token/",stian)
        self.access_token = self.response.data['access']
        self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token
        #Create workout
        objWorkout = Workout.objects.create(name="Trening", owner=self.sara, visibility="CO", notes="deertyhgfd",date="2021-03-11T9:10:10Z")
        #Create file
        objFile = WorkoutFile.objects.create(file='path', owner=self.sara, workout=objWorkout)
        #Create comment
        objComment = Comment.objects.create( owner=self.sara,  workout=objWorkout, content="Kommentar til trening", timestamp="2021-03-11T9:10:10Z")
        #Response from API
        responseWorkout = self.client.get('/api/workouts/'+str(objWorkout.id)+'/')
        responseFile = self.client.get('/api/workout-files/'+str(objFile.id)+'/')
        responseComment = self.client.get('/api/comments/'+str(objComment.id)+'/')
        #Check if correct
        self.assertEqual(responseWorkout.status_code, errorStatusCode)
        self.assertEqual(responseComment.status_code, errorStatusCode)
        self.assertEqual(responseFile.status_code, errorStatusCode)



        ''''self.client.defaults['HTTP_AUTHORIZATION']="Bearer " + self.access_token

        #Create exercise and workout
        resp = self.client.post("/api/exercises/", data={"name":"Hard Work", "description":"Training hard for a long time","unit":"Minutes"})
        self.exercise_url = resp.data['url']
        validWorkoutForm["exercise_instances"].append({"exercise":self.exercise_url,"number":20,"sets":20})
        workout = self.client.post("/api/workouts/", data=json.dumps(validWorkoutForm), content_type='application/json')
        #Retrieve workout
        res = self.client.get("/api/workouts/?ordering=name", HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
        #res.json()['results']
        res = self.client.get('/api/workouts/' + str(workout.json()['id'])+"/")
        self.assertEqual(res.status_code, 200)

        #Create comment
        obj = {"owner": "athlete","timestamp": "2021-03-11T12:30","content": "content", "workout":  "/api/workouts/"+ str(workout.json()['id'])+"/"}
        comment = self.client.post("/api/comments/", data=json.dumps(obj), content_type='application/json')
        res = self.client.get('/api/comments/' + str(comment.json()['id'])+"/")
        self.assertEqual(res.status_code, 200)
        #Create file
        workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=workout.json(), visibility="PR")
        print(workout)
        file = WorkoutFile.objects.create(file='path/file', owner="athlete", workout=workout)
        res = self.client.get('/api/workout-files/' + str(file.id)+"/")
        self.assertEqual(res.status_code, 200)'''

        #self.assertEqual(True, False)
        #Opprette to brukere
        # Eine sender forespørsel på å få coach
        # Legge til ein workouts
        # hente workouten og sjekke at brukeren kan se alt
        # Bruker skal kunne se: detaljer, filer, kommentarer




"""
Start of boundary value test for registration form
"""


validRegisterForm = {
	"username":"test",
	"password":"password",
	"password1":"password",
	"email":"mail@mail.no",
	"country":"Norway",
	"phone_number":"12345678",
	"city":"Trondheim",
	"street_address":"Kongens Gate 12"
}

def generate_string(length):
	return "".join(choice(ascii_lowercase) for i in range(length))


class RegisterBoundaryTest(TestCase):
	
	def test_valid_register(self):
		#Test with every field with normal values
		response = self.client.post("/api/users/",validRegisterForm)
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

	def test_username(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""
		form = copy.copy(validRegisterForm)
		"""
		#Test too short username
		form['username'] = generate_string(3)
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)
		"""

		#Test min length of username
		form['username'] = generate_string(4)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)
		
		#Test min length of username
		form['username'] = generate_string(25)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		"""
		#Test too short username
		form['username'] = generate_string(26)
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)
		"""

	def test_password(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""
		form = copy.copy(validRegisterForm)
		"""
		#Test too short password
		form['username'] = "user1"
		form['password'] = generate_string(7)
		form['password1'] = form['password']
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)
		"""
		
		#Test min length of password
		form['username'] = "user2"
		form['password'] = generate_string(8)
		form['password1'] = form['password']
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)


		#Test max length of password
		form['username'] = "user3"
		form['password'] = generate_string(25)
		form['password1'] = form['password']
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		"""
		#Test too long password
		form['username'] = "user4"
		form['password'] = generate_string(26)
		form['password1'] = form['password']
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)
		"""
	

	def test_email(self):
		form = copy.copy(validRegisterForm)
		#Testing wrong email prefix
		form['username'] = "user2"
		form['email'] = "@mail.com"
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)

		#Testing wrong email domain
		form['username'] = "user3"
		form['email'] = "mail@.com"
		response = self.client.post("/api/users/",form)
		#Should not create a new user
		self.assertEqual(response.status_code, 400)

	def test_phonenumber(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""
		form = copy.copy(validRegisterForm)

		"""
		#Testing wrong email prefix
		form['username'] = "user2"
		form['phone'] = "A9080302"
		response = self.client.post("/api/users/",form)
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)

		#Testing wrong email domain
		form['username'] = "user3"
		form['phone'] = "AAAAAAA6"
		response = self.client.post("/api/users/",form)
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""

	
	def test_country(self):
		#Testing empty country
		form = copy.copy(validRegisterForm)
		form['username'] = "user2"
		form['country'] = ""
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		#Test max length of address
		form['username'] = "user3"
		form['country'] = generate_string(50)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		"""
		#Test more than max length of address
		form['username'] = "user4"
		form['country'] = generate_string(51)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 400)
		"""



	def test_city(self):
		#Testing empty city
		form = copy.copy(validRegisterForm)
		form['username'] = "user2"
		form['city'] = ""
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		#Test max length of address
		form['username'] = "user3"
		form['city'] = generate_string(50)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		"""
		#Test more than max length of address
		form['username'] = "user4"
		form['city'] = generate_string(51)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 400)
		"""


	def test_address(self):
		#Testing empty address
		form = copy.copy(validRegisterForm)
		form['username'] = "user2"
		form['street_address'] = ""
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		#Test max length of address
		form['username'] = "user3"
		form['street_address'] = generate_string(50)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 201)

		"""
		#Test more than max length of address
		form['username'] = "user4"
		form['street_address'] = generate_string(51)
		response = self.client.post("/api/users/",form)
		#Should create a new user
		self.assertEqual(response.status_code, 400)
		"""



"""
End of boundary value test for registration form

Start of boundary value test for workout form
"""
        

class WorkoutBoundaryTest(TestCase):
	@classmethod
	def setUpTestData(cls):
		
		cls.my_client = Client()

		cls.my_client.post("/api/users/", validRegisterForm)
		response = cls.my_client.post("/api/token/",{"username":"test","password":"password"})
		cls.access_token = response.data['access']
		resp = cls.my_client.post("/api/exercises/", data={"name":"Hard Work", "description":"Training hard for a long time","unit":"Minutes"}, HTTP_AUTHORIZATION="Bearer " + cls.access_token)
		cls.exercise_url = resp.data['url']
		validWorkoutForm["exercise_instances"].append({"exercise":cls.exercise_url,"number":20,"sets":20})


	def test_all_valid_fields(self):
		# Test every field's normal value
		response = self.my_client.post("/api/workouts/", data=json.dumps(validWorkoutForm), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)


	def test_name(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""
		
		form = copy.deepcopy(validWorkoutForm)
		#Test empty name
		form['name'] = ""
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)

		#Test min length of name
		form['name'] = generate_string(1)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		#Test max length of name
		form['name'] = generate_string(25)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		"""
		#Test too long name
		form['name'] = generate_string(26)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""


	def test_date(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""

		#Test min date
		form = copy.deepcopy(validWorkoutForm)
		form['date'] = "1970-01-01T00:00"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		#Test invalid Hours
		form['date'] = "2012-12-30T25:30"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)

		"""
		#Test less than min date
		form['date'] = "1969-12-30T23:59"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""

		#Test max date
		form['date'] = "9999-12-31T23:59"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		#Test more than max date
		form['date'] = "10000-01-01T00:00"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)


		#Test invalid month
		form['date'] = "2012-13-20T08:30"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)

		#Test invalid day of month
		form['date'] = "2012-12-32T08:30"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)


		#Test invalid Minutes
		form['date'] = "2012-12-30T23:61"
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)


	def test_notes(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""

		#Test empty notes
		form = copy.deepcopy(validWorkoutForm)
		form['notes'] = ""
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 400)

		#Test min length of notes
		form['notes'] = generate_string(1)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		#Test max length of notes
		form['notes'] = generate_string(300)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should create a new workout
		self.assertEqual(response.status_code, 201)

		"""
		#Test more than max length of notes
		form['notes'] = generate_string(301)
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""


	def test_number_exercise(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""

		form = copy.deepcopy(validWorkoutForm)
		
		#Test with empty number
		form['exercise_instances'][0]['number'] = ""
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		

		"""
		#Test zero value of number
		form['exercise_instances'][0]['number'] = 0
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""
		
		"""
		#Test negative value of number
		form['exercise_instances'][0]['number'] = -1
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""

	def test_sets_exercise(self):
		"""
		The tests are commented out to pass tests when pushing it to GitLab.
		The limitation arent implemented yet, some of the tests will fail.
		"""

		form = copy.deepcopy(validWorkoutForm)

		#Test with empty sets
		form['exercise_instances'][0]['sets'] = ""
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)

		"""
		#Test zero value of sets
		form['exercise_instances'][0]['sets'] = 0
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""

		"""
		#Test negative value of sets
		form['exercise_instances'][0]['sets'] = -1
		response = self.my_client.post("/api/workouts/", data=json.dumps(form), HTTP_AUTHORIZATION="Bearer " + self.access_token, content_type='application/json')
		#Should not create a new workout
		self.assertEqual(response.status_code, 400)
		"""

	






